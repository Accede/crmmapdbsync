<?php

namespace CRMMap;

class API extends Base
{

    const DEFAULTURL = "https://maps.crm-map.com/api.php";
    protected $apikey = '';
    /**
     * Get the value of key
     */
    public function getKey()
    {
        if (empty($this->apikey)) {
            $this->apikey = $this->container['config']->get("APIKey");
        }
        return $this->apikey;
    }

    public function overrideKey($newkey){
        $this->apikey = $newkey;
    }

    public function getBaseUrl()
    {
        return $this->container['config']->get("APIHost", self::DEFAULTURL);
    }

    public function customfields(){
        return $this->call('setup/customfields');
    }

    private function checkCurl($ch ) {

        if(curl_error($ch))
        {
            throw new \Exception ("curl_error:  ".curl_error($ch));
     
        }



        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if (!( $httpCode==200)) {
            throw new \Exception ("HTTP Error:  $httpCode");
        }

        return true;

    }


    public function getfile($name, $saveto) {
        set_time_limit(0);
        $fp = fopen ($saveto, 'w+');

        $url = str_replace(" ","%20",$this->getBaseUrl() . '/files/'.$name);
        $ch = curl_init($url);

 
        $this->wrtieln("API Call URL: $url", true);


        curl_setopt($ch, CURLOPT_TIMEOUT, 50);
        curl_setopt($ch, CURLOPT_FILE, $fp); 
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        $headers = [
            'Cache-Control: no-cache',
            'User-Agent: CRMMap API DB Sync',
            'apikey: ' . $this->getKey(),
        ];

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);


        curl_exec($ch); 


        $this->checkcurl($ch);
     

        curl_close($ch);
        fclose($fp);

        return true;
    }

    public function delete($what, $code) {
        
        $this->wrtieln("API delete: $what", true);
        
        if (empty($code)) {
            throw new \Exception('Code required for delete');        
        }
        
        $url = '';
        switch (strtolower($what)) {
            case 'items':
            $this->wrtieln("items delete not ready");
                break;
            case 'contacts':
            $this->wrtieln("contacts delete not ready");
                break;
            case 'transactions':
               
                $url = "transactions/".$code."/delete";

             break;    
            default:
                # code...
                break;
        }
        if (!empty($url)) {
            $this->call($url, [], $data);
        }
    }

    public function update( $what, $data)
    {
        $this->wrtieln("API update: $what", true);
        
        $url = '';
        switch (strtolower($what)) {
            case 'items':
                if (!isset($data['code'])) {
                    throw new \Exception('Code required for product');
                }
                $prodcode = urlencode($data['code']);
                $url = "products/$prodcode";
                break;
            case 'contacts':
                if (!isset($data['contactcode'])) {
                    throw new \Exception('contactcode required for product');
                }
                $contactcode = urlencode($data['contactcode']);
                $url = "contacts/$contactcode";
                break;
            case 'transactions':
                $code = '';
                if (isset($data['code'])) {
                    $code = "/".urlencode($data['code']);
                    unset($data['code']);
                }
               
               
               
                $url = "transactions".$code;

             break;    
            default:
                # code...
                break;
        }
        if (!empty($url)) {
            $this->call($url, [], $data);
        }

    }

    public function exportlast( $what, $age)
    {

        
        $this->wrtieln("API exportlast: $what, $age", true);
     


        $url = '';
        $param = [];
        switch (strtolower($what)) {
            case 'items':
  
                break;
            case 'contacts':
                 $url = "contacts/export";
 
                  break;
            case 'transactions':
          
                $url = "transactions/export";

             break;    
            default:
                # code...
                break;
        }

        if (!empty($age)) {
            $param['age'] = $age;
        }

        if (empty($url)) {
            throw new \Exception("exportLast: $what no supported");
        }
        return $this->call($url, $param);


    }

    public function Call( $func, $params = [], $post = null)
    {
        $ch = curl_init();
        $url = $this->getBaseUrl() . (!empty($func) ? "/$func" : "");
        if ($post) {
            //curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post, '', '&'));
        }

        if (!$post) {
            if (!empty($params )) {
                $url =  $url . '?' .  http_build_query($params, '', '&');
            }
        }


        $this->wrtieln("API Call URL: $url", true);
     

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $headers = [
            'Cache-Control: no-cache',
            'User-Agent: CRMMap API DB Sync',
            'apikey: ' . $this->getKey(), //not currently supported
        ];

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $server_output = curl_exec($ch);
        $this->checkcurl($ch);

        $this->wrtieln($server_output, true);

        //todo check 400's etc
        if($server_output === false)
		{
			throw new \Exception('Curl error: ' . curl_error($ch));
		}
		

        curl_close($ch);

        $object = json_decode($server_output);

        if (isset($object->error)) {
            throw new Exception($object->error);
        }
        return $object;
    }

}
