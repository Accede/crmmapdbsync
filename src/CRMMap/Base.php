<?php

namespace CRMMap;

use Symfony\Component\Console\Output\OutputInterface;

class Base
{

    public $container = null;

    public function __construct($container)
    {
        $this->container = $container;

    }

    public function wrtieln($msg, $debug = false)
    {

        if (isset($this->container['output'])) {
            $level = ($debug) ? OutputInterface::VERBOSITY_DEBUG : OutputInterface::VERBOSITY_NORMAL;
            $this->container['output']->writeln(
                $msg,
                $level
            );
            return $this;
        }

        echo "$msg \n";
    }

}
