<?php

namespace CRMMap\Command\API;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;



class KeySet extends Command{

    const Name = "API:SetKey";


    protected function configure(){
        $this->setName(self::Name)
                ->setDescription("Set the CRMMap API Key")
                ->addArgument('APIKey', InputArgument::REQUIRED, '');
    }

    protected function execute(InputInterface $input, OutputInterface $output){
        $this->getApplication()->container['output'] = $output;

        $APIKey = $input->getArgument('APIKey');

        $this->getApplication()->container['config']->set("APIKey", $APIKey)->save();
        $output->writeln('Your Key has been set');



        
    }

}