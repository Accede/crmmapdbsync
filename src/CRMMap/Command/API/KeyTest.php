<?php

namespace CRMMap\Command\API;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Style\SymfonyStyle;


class KeyTest extends Command{


    const Name = "API:TestKey";


    protected function configure(){
        $this->setName(self::Name)
        ->setDescription("Test you CRMMap API Key");
    }

    protected function execute(InputInterface $input, OutputInterface $output){
        $this->getApplication()->container['output'] = $output;

        $API = $this->getApplication()->container['api'];
        $versioninfo =  $API->call('');
        $io = new SymfonyStyle($input, $output);
        $io->title( $versioninfo->name );
        $io->write("Version: " .$versioninfo->version);
        $io->success("Test complete");
    }

}