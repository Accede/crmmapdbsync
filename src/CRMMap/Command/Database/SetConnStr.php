<?php

namespace CRMMap\Command\Database;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Style\SymfonyStyle;


class SetConnStr extends Command{


    const Name = "Database:Config";



    use LockableTrait;

    protected function configure(){
        $this->setName(self::Name)
        ->setDescription("Setup for the database connection")
        ->addArgument('constr', InputArgument::OPTIONAL, 'PHP PDO Connection String')
        ->addArgument('name', InputArgument::OPTIONAL, 'Database User name')
        ->addArgument('password', InputArgument::OPTIONAL, 'Database Password')
        ;
            
    }

    protected function execute(InputInterface $input, OutputInterface $output){
        $this->getApplication()->container['output'] = $output;

        if (!$this->lock()) {
            $output->writeln('The command is already running in another process.');

            return 0;
        }
        $Database =  $this->getApplication()->container['database'];
        $io = new SymfonyStyle($input, $output);
        $io->title('Database Config');
       
        $constr   = $input->getArgument('constr');
        $username = $input->getArgument('name');
        $password = $input->getArgument('password');
        $config =  $this->getApplication()->container['config'];

        while (empty($constr)) {
            
           $type= $io->choice('What type of PDO Connection', array('MYSQL','MSSQL', 'custom'),'custom');
           $io->writeln( $type);
            switch ($type) {
                case "MYSQL":
                $db_host = $io->ask('Host IP: ');
                $db_name = $io->ask('Database Name');
                $constr = 'mysql:host='.$db_host.'; dbname='.$db_name;
                    break;

                case "MSSQL":
                $db_host = $io->ask('Host IP: ');
                $db_name = $io->ask('Database Name');
                $constr = "sqlsrv:Server=$db_host;Database=$db_name";
                    break;
            
                default:

                
                $constr = $io->ask('PDO Connection String',  $config->get('DBConstr'));
                    break;
            }

            $io->writeln($constr);
           if (!$io->confirm('Does this look correct?', true)) {
            $constr = '';
           }
            


        }

        while (empty($username)) {
            $username = $io->ask('Username',  $config->get('DBUsername'));
        }
     
        while (empty($password)) {
            $password = $io->ask('password',  $config->get('DBPassword'));
        }
        if ($io->confirm('Test?', true)) {
            $Database->loadsettings($constr,$username,$password);
            $Database->connect();
            $io->writeln("Connected");
        }

        if ($io->confirm('Save?', true)) {
            $this->getApplication()->container['config']
                ->set("DBUsername", $username)
                ->set("DBPassword", $password)
                ->set("DBConstr", $constr)
                ->save();

           }
     
    }

}