<?php

namespace CRMMap\Command\Database;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class Test extends Command
{

    const Name = "Database:Test";

    use LockableTrait;

    protected function configure()
    {
        $this->setName(self::Name)
            ->setDescription("Test Setup for the database connection")

        ;

    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->getApplication()->container['output'] = $output;

        $Database = $this->getApplication()->container['database'];
        $io = new SymfonyStyle($input, $output);
        $io->title('Database Config Test');

        $Database->loadsettings();
        $Database->connect();
        $io->writeln("Connected");
        $Database->test();

    }

}
