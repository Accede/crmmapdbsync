<?php

namespace CRMMap\Command\File;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class Get extends Command
{

    const Name = "File:Get";

    use LockableTrait;

    protected function configure()
    {
        $this->setName(self::Name)
            ->setDescription("Download Single File")
            ->addArgument('name', InputArgument::REQUIRED, 'File Namee')
            ->addArgument('save', InputArgument::OPTIONAL, 'Save File Namee')
            ->addOption('dir', null, InputOption::VALUE_OPTIONAL, 'Save where')
            ->addOption('overwrite', null, InputOption::VALUE_NONE, 'Overwrite existing file')
            ->addOption('ignore', null, InputOption::VALUE_NONE, 'ignore existing file')

        ;

    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $config = $this->getApplication()->container['config'];

        $this->getApplication()->container['output'] = $output;

        $io = new SymfonyStyle($input, $output);
    
        $name = $input->getArgument('name');
        $save = $input->getArgument('save');

        $dir = $input->getOption('dir');
        $overwrite = $input->getOption('overwrite');
        $ignore = $input->getOption('ignore');


        if (empty($save)) {
            $save = $name;
        }

        if (!empty( $dir ) ) {
           $basename = basename($save);

           $save = $dir. '/'. $basename;

        }

        if (!$overwrite) {
            if (file_exists($save)) {
                if ($ignore) {
                    $io->block($save, 'Ignored', 'fg=black;bg=green', ' ', true);
                    return true;
                }
                throw new \Exception ("File already exists, $save");
            }
        }

        $this->getApplication()->container['api']->getfile($name, $save);


        $io->block($save, 'Saved', 'fg=black;bg=green', ' ', true);


    }

}
