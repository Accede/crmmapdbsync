<?php

namespace CRMMap\Command\File;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Style\SymfonyStyle;


class ListAll extends Command{


    const Name = "File:ListAll";



    use LockableTrait;

    protected function configure(){
        $this->setName(self::Name)
        ->setDescription("List All files")
        
        ;
            
    }

    protected function execute(InputInterface $input, OutputInterface $output){
        
        $config = $this->getApplication()->container['config'];
       
        $this->getApplication()->container['output'] = $output;

        
        $io = new SymfonyStyle($input, $output);
        $io->title('Files  List');
    
        $files =[] ;

        foreach ($this->getApplication()->container['api']->Call("files") as $file) {
           $files[] = (array)$file;
        }

        $io->table(["Type", "Code", "Named","Size"], $files);
        
        
    }

}