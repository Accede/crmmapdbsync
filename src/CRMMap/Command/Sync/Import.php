<?php

namespace CRMMap\Command\Sync;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Style\SymfonyStyle;


class Import extends Command{


    const Name = "Sync:Import";



    use LockableTrait;

    protected function configure(){
        $this->setName(self::Name)
        ->setDescription("Run the Import Sync")
        ->addArgument('name', InputArgument::REQUIRED, 'Sync Named')
       
        ;
            
    }

    protected function execute(InputInterface $input, OutputInterface $output){
        
        $config = $this->getApplication()->container['config'];
       
        $this->getApplication()->container['output'] = $output;
        
        $io = new SymfonyStyle($input, $output);
        $io->title('Import');
      

        $name   = $input->getArgument('name');
    
        if (!file_exists( $name)) {
            $filename = $config->get('ConfigDir')."/$name";

            if (!file_exists( $filename)) {
                $filename = $config->get('ConfigDir')."/$name.yaml";
            }
        }
     

        if (!file_exists( $filename)) {
            throw new \Exception ("File not found, $name");
        }


        $sync = new \CRMMap\Sync($this->getApplication());
        $sync->output = $output;
         $sync->setConfigFile($filename)->load();
        $sync->ImportLast($output);
     
        
        
    }

}