<?php

namespace CRMMap\Command\Sync;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class NewConfig extends Command
{

    const Name = "Sync:New";

    use LockableTrait;

    protected function configure()
    {
        $this->setName(self::Name)
            ->setDescription("Create new Sync config")
            ->addArgument('type', InputArgument::OPTIONAL, 'Type: [Items,Contacts,Transactions,Users]')

        ;

    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->getApplication()->container['output'] = $output;

        $config = $this->getApplication()->container['config'];
        $database = $this->getApplication()->container['database'];
        $type = $input->getArgument('type');

        $io = new SymfonyStyle($input, $output);
        $io->title('New Sync Config');

        $types = array('Items', 'Contacts', 'Transactions', 'Users');

        if (!in_array($type, $types)) {
            $type = $io->choice('Sync What?', $types);
        }

        $filename = $config->get('ConfigDir') . "/$type.yaml";
        $i = 0;
        while (file_exists( $filename )) {
            $i++;
            $filename = $config->get('ConfigDir') . "/{$type}_{$i}.yaml";
        }
        $sync = new \CRMMap\Sync($this->getApplication());
        $sync->output = $output;
         $sync->setConfigFile($filename);
        $sync->set("type", $type);
        $sync->set("created", (new \DateTime)->format('c'));
        $sync->defaultmatch(); // set the default matches
        $sync->store(); //store the blank querys
        $sync->save();

        $io->writeln($type . ' Config stored, ' . $filename);
        $io->writeln("---");

    }

}
