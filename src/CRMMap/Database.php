<?php

namespace CRMMap;

use \PDO;

class Database extends Base
{

    protected $DBConstr = null;
    protected $DBUsername = null;
    protected $DBPassword = null;

    public function loadsettings($DBConstr = null, $DBUsername = null, $DBPassword = null)
    {
        $config = $this->container['config'];
        $this->DBConstr = (empty($DBConstr) ? $config->get('DBConstr') : $DBConstr);
        $this->DBUsername = (empty($DBUsername) ? $config->get('DBUsername') : $DBUsername);
        $this->DBPassword = (empty($DBPassword) ? $config->get('DBPassword') : $DBPassword);
    }

    public function DBType()
    {
        if (empty($this->DBConstr)) {
            return false;
        }

        return strtolower(explode(":", $this->DBConstr)[0]);
    }

    public function test() {
        $con = $this->connect();
        switch (strtolower($this->DBType())) {
            case 'mysql':
                $sth = $con->prepare('show tables;');
                $sth->execute();
                $tables = $sth->fetchAll();

                $this->wrtieln("Database tables found: ". sizeof($tables));           
                break;
            
            default:
                $this->wrtieln("No test for DB Type: ".$this->DBType());
                break;
        }

    }

    private $con;

    public function connect()
    {
        if (!empty($this->con)) {
            return $this->con;
        }
        if (empty($this->DBConstr)) {
            $this->loadsettings();
        }

        $this->con = new PDO($this->DBConstr, $this->DBUsername, $this->DBPassword);
        $this->con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        if ($this->DBType()=='mysql') {
            $this->con->exec("SET CHARACTER SET utf8");
        }

        return $this->con;
    }

}
