<?php 


namespace CRMMap;

class Exception extends \Exception
{
    public function __construct (  $message = "" , $code = 0 ,  $previous = NULL  ) {
        $message = "CRMMap API Error: ".$message;
        parent::__construct($message,$code,$previous);
    }
}