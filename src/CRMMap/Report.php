<?php

namespace CRMMap;

class Report extends Config
{

    public function setType($what, $how)
    {
        $this->set('What', $what);
        $this->set('how', $how);
        $this->set('started', (new \Datetime())->format('c'));

        return $this;
    }

    public function setExpecting($count)
    {
        echo "Expecting: " . $count . "\n";
        $this->set("Expecting", $count);
        return $this;
    }

    public function addResult($pass, $msg = '')
    {
        $Errors = $this->get('Errors', []);
        $TotalCount = $this->get('TotalCount', 0);
        $TotolPass = $this->get('TotolPass', 0);
        $TotalErrors = $this->get('TotalErrors', 0);
        $TotalCount++;

        if ($pass) {
            echo ".";
            $TotolPass++;
        }
        if (!$pass) {
            echo "\n{$msg}\n";
            $Errors[] = $msg;
            $TotalErrors++;
        }

        $this->set('Errors', $Errors);
        $this->set('TotalCount', $TotalCount);
        $this->set('TotolPass', $TotolPass);
        $this->set('TotalErrors', $TotalErrors);
        $this->set('updated', (new \Datetime())->format('c'));
        return $this;
    }

    public function finished()
    {
        $this->set('finished', (new \Datetime())->format('c'));
        return $this;
    }

    public function filename() {
        return $this->container['config']->get('LogDir') . "/last_{$this->get('What')}_{$this->get('how')}.yaml";
    }
    public function save($filename = '')
    {

        $this->set('saved', (new \Datetime())->format('c'));
        if (empty($filename)) {
            $filename = $this->filename();
        }


        if (!empty($this->get('finished'))) {
            echo "\n Report Saved: $filename  \n";
        }
        return parent::save($filename);
    }

}
