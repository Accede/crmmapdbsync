<?php

namespace CRMMap;
use Symfony\Component\Console\Input\ArrayInput;
class Sync extends Config
{

    protected $all = "";
    protected $check = '';
    protected $update = '';

    protected $match = '';

    protected $syncclassname = '';
    protected $syncclass = null;

    protected $application = null;

    public function __construct($application)
    {
        $this->application = $application;
        parent::__construct($application->container);
    }

  

    public function load()
    {
        $querys = $this->get('querys');
        $this->all = (isset($querys['all'])) ? $querys['all'] : null;
        $this->check = (isset($querys['check'])) ? $querys['check'] : null;
        $this->update = (isset($querys['update'])) ? $querys['update'] : null;
     
        $this->match = $this->get('match');
        $this->syncclassname = $this->get('syncclass');

       
    }

    public function getSyncClass() {
        
        if (!empty($this->syncclass)) {
            return $this->syncclass;
        }
        if (!empty($this->syncclassname)) {
            if (class_exists($this->syncclassname, true)) {
                $classname = $this->syncclassname;
                $this->syncclass = new $classname($this);
                return $this->syncclass;
            }
        }

        return false;
    }

    public function store()
    {
        $querys = [];
        $querys['all'] = $this->all;
        $querys['check'] = $this->check;
        $querys['update'] = $this->update;
        $this->set('querys', $querys);
        $this->set('match', $this->match);
        $this->set('syncclass', $this->syncclassname);

    }

    public function defaultmatch()
    {
        $theType = strtolower($this->get('type'));

        $feilds = $this->container['api']->customfields();
        if (!isset($feilds->{$theType})) {
            throw new Exception("Failed for get fields for $theType from the API");
        }

        $matchfields = $feilds->{$theType};

        $match = [];
        $match['CustomFields'] = [];
        switch ($theType) {
            case 'items':

                $match['code'] = '';
                $match['category'] = '';
                $match['name'] = '';
                $match['description'] = '';
                $match['value'] = '';
                break;
            case 'contacts':

                $match['contactcode'] = '';
                $match['name'] = '';
                $match['address'] = '';

                break;

            case 'transactions':
                $match['code'] = '';
                $match['Status'] = '';
                $match['Products'] = [];
                $match['address'] = '';
                $match['contactcode'] = '';
                break;

            default:
                throw new exception("Unknown Type ($theType) in Sync Config");
                break;
        }

        if (isset($matchfields->CustomFields)) {
            foreach ($matchfields->CustomFields as $value) {
                $match['CustomFields'][$value->Slug] = '';
            }
        }

        $this->match = $match;

    }

    public function matchdata($data)
    {
        $match = $this->match;
        //todo: custom fields in Match
        foreach ($match as $key => $value) {
            if (!is_array($value) and (!empty($value))) {
                $match[$key] = (isset($data[$value])) ? $data[$value] : $value;
            }
        }

        if (isset($match['CustomFields'])) {
            foreach ($match['CustomFields'] as $key => $value) {
                $match['CustomFields'][$key] = (isset($data[$value])) ? $data[$value] : $value;
            }
        }

        return $match;
    }

    private function cleancode($code)
    {
        $code = str_replace("/", "", $code);
        $code = str_replace("  ", " ", $code);
        return trim($code);
    }
    public function ExportAll()
    {

        if ($apikey = $this->get('APIKey')) {
            $this->container['api']->overrideKey($apikey);
        }
        $type = strtolower($this->get('type'));
        $report = $this->container['report']; //creates a new report
        $report->setType($type, 'ExportAll');

        $con = $this->container['database']->connect();
        $query = $con->prepare($this->all);
        $query->execute();

        $results = $query->fetchAll(\PDO::FETCH_ASSOC);

        $report->setExpecting(sizeof($results));
        $c = 0;
        foreach ($results as $key => $value) {
            $data = $this->matchdata($value);

            switch ($type) {
                case 'items':

                    $data['code'] = $this->cleancode($data['code']);
                    break;
                case 'contacts':
                    $data['contactcode'] = $this->cleancode($data['contactcode']);
                    break;
                case 'transactions':

                    if (!is_array($data['Products'])) {
                        $data['Products'] = $this->cleancode($data['Products']);
                    }

                    break;
                default:
                    throw new exception("Unknown Type in Sync Config [$type ]");
                    break;

            }
            try {
                $this->container['api']->update($type, $data);
                $report->addResult(true);
            } catch (\Exception $e) {
                $report->addResult(false, $e->getMessage());
            }

            $report->save();

            $c++;

            if ($c % 10 == 0) {
                echo $c;
            }

        }
        $report->finished()->save();
    }

    public function ImportLast()
    {

        if ($apikey = $this->get('APIKey')) {
            $this->container['api']->overrideKey($apikey);
        }

        $type = strtolower($this->get('type'));
        $report = $this->container['report']; //creates a new report
        $report->setType($type, 'ImportLast');

        $age = "-2 months";
        $function = "ImportLast";
        if ($importlast = $this->get('importlast')) {
            $age = isset($importlast['age'])?$importlast['age']: $age;
            $function = isset($importlast['function'])?$importlast['function']: $function;
        }

        $data =  $this->container['api']->exportlast($type, $age);

        $handled = false;
        if ($sync = $this->getSyncClass()) {
            if (method_exists($sync,  $function )) {
                $handled = $sync->{ $function }($data, $report);
            }
         }

        
        if (!$handled) {
            $report->addResult(false,"ImportLast did not complete"); 
        } 

        $report->finished()->save();
    }


    public function execute($name, $arguments= []) {

        if (!is_array($arguments)) {
            throw new \Exception ("Execute arguments needs to be an array");
        }

        if (empty($this->output)) {
            throw new \Exception("Output required");
        }
        $command = $this->application->find($name);
        $arguments ['command'] = $name;

        $input = new ArrayInput($arguments);
      
        $returnCode = $command->run($input, $this->output);

        return $returnCode;
    }


}

/*$sth = $dbh->prepare('SELECT name, colour, calories FROM fruit
WHERE calories < :calories AND colour = :colour');
$sth->bindParam(':calories', $calories)…

$sth->bindParam(':calories', $calories);
$sth->bindParam(':colour', $colour);
$sth->execute();*/
