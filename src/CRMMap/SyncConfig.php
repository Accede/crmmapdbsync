<?php

namespace CRMMap;



class SyncConfig extends Base
{

    public $sync = null;

    public function __construct($sync)
    {
        $this->sync = $sync;
        parent::__construct($sync->container);
    }


    public function execute($name, $arguments= []) {
        return $this->sync->execute($name, $arguments);
    }

}
