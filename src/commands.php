<?php

use CRMMap\Command\API\KeySet;
use CRMMap\Command\API\KeyTest;
use CRMMap\Command\Database\SetConnStr;
use CRMMap\Command\Database\Test;
use CRMMap\Command\Sync\NewConfig;
use CRMMap\Command\Sync\Export;
use CRMMap\Command\Sync\Import;
use CRMMap\Command\File\Get;
use CRMMap\Command\File\ListAll;

use Symfony\Component\Console\CommandLoader\FactoryCommandLoader;

$commandLoader = new FactoryCommandLoader(array(
    KeySet::Name => function () {return new KeySet();},
    KeyTest::Name => function () {return new KeyTest();},
    SetConnStr::Name => function () {return new SetConnStr();},
    Test::Name => function () {return new Test();},
    NewConfig::Name => function () {return new NewConfig();},
    Export::Name => function () {return new Export();},
    Import::Name => function () {return new Import();},
    Get::Name => function () {return new Get();},
    ListALl::Name => function () {return new ListAll();},
));

$app->setCommandLoader($commandLoader);
