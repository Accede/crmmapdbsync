<?php

$container = new  Pimple\Container();
$app->container = $container;


$container['config'] = function ($c) {
    $config = new \CRMMap\Config($c);
    $config->setConfigFile(__DIR__.'/../config.yaml');
    $config->set('ConfigDir', realpath(__DIR__.'/../configs/'));
    $config->set('LogDir', realpath(__DIR__.'/../logs/'));
    return $config;
};


$container['database'] = function ($c) {
    $database = new \CRMMap\Database($c);
    return $database;
};

$container['api'] = function ($c) {
    $api = new \CRMMap\API($c);
    return $api;
};


$container['report'] = $container->factory(function ($c) {
    return new \CRMMap\Report($c);
});
